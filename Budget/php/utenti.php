<?php
require 'connect.php';

$stmt = $conn->prepare("SELECT id,utente FROM utenti");
$stmt->execute();
$result = $stmt->get_result();
$outp = $result->fetch_all(MYSQLI_ASSOC);

echo json_encode($outp);
$conn->close();
?>
