<?php
require 'connect.php';

$stmt = $conn->prepare("SELECT id,fornitore FROM fornitori");
$stmt->execute();
$result = $stmt->get_result();
$outp = $result->fetch_all(MYSQLI_ASSOC);

echo json_encode($outp);
$conn->close();
?>
