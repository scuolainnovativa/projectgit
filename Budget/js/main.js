var error=false;
var flag = false;
var selezionata;
$(function(){
  $('#tt').datagrid({
    title:'Budget',
    rownumbers:true,
    iconCls:'icon-edit',
    width:1400,
    height:600,
    autosave:true,
    toolbar:'#tb',
    idField:'id',
    loadMsg:'Caricamento...',
    singleSelect:true,
    scrollOnSelect:true,
    sortName:'id',
    fitColumns:false,
    remoteSort:false,
    url:'php/getbudget.php',
    columns:[[
      {field:'id',title:'ID', editable:false, sortable:true},
      {field:'utente',title:'Utente',
        formatter:function(value,row){
          return row.utente;
        },
        editor:{
          type:'combobox',
          options:{
            url:'php/utenti.php',
            valueField:'id',
            textField:'utente',
            required:true,
            editable:false
          }
        }, sortable:true
      },
    {field:'azienda',title:'Azienda',
      formatter:function(value,row){
        return row.azienda;
      },
      editor:{
        type:'combobox',
        options:{
          url:'php/aziende.php',
          valueField:'id',
          textField:'nome',
          required:true,
          editable:false
        }
      }, sortable:true
    },
    {field:'categoria',title:'Categoria',
      formatter:function(value,row){
        return row.categoria;
      },
      editor:{
        type:'combobox',
        options:{
          url:'php/categorie.php',
          valueField:'id',
          textField:'categoria',
          required:true,
          editable:false
        }
      }, sortable:true
    },
    {field:'cod',title:'Codice articolo',
      formatter:function(value,row){
        return row.cod;
      },
      editor:{
        type:'combobox',
        options:{
          url:'php/codici.php',
          valueField:'id',
          textField:'codArticolo',
          required:true,
          editable:false
        }
      }, sortable:true
    },
    {field:'descrizione',title:'Descrizione',height:40,editor:'text'},
    {field:'necessario',title:'Necessario',sortable:true,align:'right',editor:{type:'numberbox',options:{precision:2,required:true}}},
    {field:'facoltativo',title:'Facoltativo',align:'right',editor:{type:'numberbox',options:{precision:2}}},
    {field:'fornitore',title:'Fornitore',
      formatter:function(value,row){
        return row.fornitore;
      },
      editor:{
        type:'combobox',
        options:{
          url:'php/fornitori.php',
          valueField:'id',
          textField:'fornitore',
          required:true,
          editable:false
        }
      }, sortable:true
    },
    {field:'note',title:'Note',editor:'text'}
    ]],
    onEndEdit:function(index,row){
      var ed = $(this).datagrid('getEditor', {
        index: index,
        field: 'utente'
      });
      row.utente = $(ed.target).combobox('getText');

      var ed = $(this).datagrid('getEditor', {
        index: index,
        field: 'azienda'
      });
      row.azienda = $(ed.target).combobox('getText');

      var ed = $(this).datagrid('getEditor', {
        index: index,
        field: 'categoria'
      });
      row.categoria = $(ed.target).combobox('getText');

      var ed = $(this).datagrid('getEditor', {
        index: index,
        field: 'cod'
      });
      row.cod = $(ed.target).combobox('getText');
      var ed = $(this).datagrid('getEditor', {
        index: index,
        field: 'fornitore'
      });
      row.fornitore = $(ed.target).combobox('getText');
      $("#btAdd").css("display","inline");
      $("#btRemove").css("display","inline");
      $("#btSave").css("display","none");
      $("#btCancel").css("display","none");
      $(this).datagrid('refreshRow', index);
    },
    onBeforeEdit:function(index,row){
      row.editing = true;
      $("#btAdd").css("display","none");
      $("#btRemove").css("display","none");
      $("#btSave").css("display","inline");
      $("#btCancel").css("display","inline");
      $(this).datagrid('refreshRow', index);
      selezionata = findRowSelected();
    },
    onAfterEdit:function(index,row){
      row.editing = false;
      $('#tt').datagrid('refreshRow', index);
      $('#tt').datagrid('reload');

    },
    onCancelEdit:function(index,row){
      row.editing = false;
      $("#btAdd").css("display","inline");
      $("#btRemove").css("display","inline");
      $("#btSave").css("display","none");
      $("#btCancel").css("display","none");
      $(this).datagrid('refreshRow', index);
    },
    onDblClickRow:function(index,row){
      $('#tt').datagrid('beginEdit', index);
    }
  });
});

function getRowIndex(){
  var tr = $(target).closest('tr.datagrid-row');
  return parseInt(tr.attr('datagrid-row-index'));
}
function findRowSelected(){
  var selectedrow = $("#tt").datagrid("getSelected");
  var rowIndex = $("#tt").datagrid("getRowIndex", selectedrow);
  return rowIndex
}
function editrow(){
  $('#tt').datagrid('selectRow', getRowIndex(target));
  $('#tt').datagrid('beginEdit', getRowIndex(target));
  flag=false;
}
function deleterow(){
  $.messager.confirm('Conferma','Sei sicuro?',function(r){
    if (r){
      var row = findRowSelected();
      var id = $('#tt').datagrid('getRows')[row].id;
      window.open("php/deleteRow.php?w1=" + id,'_blank');
      $('#tt').datagrid('deleteRow', row);
    }
  });
}
function saverow(){
  error = false;
  var row = selezionata;
  $('#tt').datagrid('endEdit', row);
  // !!Da migliorare con una funzione
  var id = $('#tt').datagrid('getRows')[row].id;
  id = checkNull(id);
  var utente = $('#tt').datagrid('getRows')[row].utente;
  requi(utente);
  utente = "'"+utente+"'";
  var azienda = $('#tt').datagrid('getRows')[row].azienda;
  requi(azienda);
  azienda = "'"+azienda+"'";
  var categoria = $('#tt').datagrid('getRows')[row].categoria;
  requi(categoria);
  categoria = "'"+categoria+"'";
  var cod = $('#tt').datagrid('getRows')[row].cod;
  requi(cod);
  cod = "'"+cod+"'";
  var descrizione = $('#tt').datagrid('getRows')[row].descrizione;
  descrizione = checkNull(descrizione);
  var necessario = $('#tt').datagrid('getRows')[row].necessario;
  requi(necessario);
  var facoltativo = $('#tt').datagrid('getRows')[row].facoltativo;
  facoltativo = checkNull(facoltativo);
  var fornitore = $('#tt').datagrid('getRows')[row].fornitore;
  requi(fornitore);
  fornitore = "'"+fornitore+"'";
  var note = $('#tt').datagrid('getRows')[row].note;
  note = checkNull(note);
  var str=id + ', ' + utente + ', ' + azienda + ', ' + categoria + ', ' + cod + ', ' + descrizione + ', ' + necessario + ', ' + facoltativo + ', ' + fornitore + ', ' + note;
  if(!error){
    window.open("php/setRow.php?w1=" + str,'_blank');
  }
  flag=false;
  location.reload();

}
function cancelrow(){
  var row = selezionata;
  var id = $('#tt').datagrid('getSelected').id;
  if(id == null){
    $('#tt').datagrid('deleteRow', row);
  } else{
    $('#tt').datagrid('cancelEdit', row);
  }
  flag=false;
}
function insert(){
  if(!flag){
    flag=true;
    var row = $('#tt').datagrid('getSelected');
    if (row){
      var index = $('#tt').datagrid('getRowIndex', row);
    } else {
      index = 0;
    }
    $('#tt').datagrid('insertRow', {
      index: index,
      row:{
        utente:''
      }
    });
    $('#tt').datagrid('selectRow',index);
    $('#tt').datagrid('beginEdit',index);
  }
}
function requi(param){
  if(param == null){
    error=true;
  }
}

function checkNull(param){
  if (param == null || param == ''){
    var temp=null;
    return temp;
  } else {
    var temp = "'"+param+"'";
    return temp;
  }
}
